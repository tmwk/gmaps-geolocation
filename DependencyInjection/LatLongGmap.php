<?php
/**
 * Created by PhpStorm.
 * User: Mario
 * Date: 21/08/14
 * Time: 23:25
 */

namespace Tmwk\GMapsBundle\DependencyInjection;


class LatLongGmap
{
    private $latitud = 0;
    private $logintud = 0;
    private $api_key;
    private $error = array();
    private $status = false;

    /**
     * LatLongGmap constructor.
     *
     * @param $api_key
     */
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }


    /**
     * @param $address
     * @param $commune
     * @param $city
     *
     * @return $this
     */
    public function address($address, $commune, $city)
    {
        $direccion = array($address, $commune, $city);
        $resultado = file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?key=%s&sensor=true&address=%s', $this->api_key, urlencode(implode(', ', $direccion))));
        $resultado = json_decode($resultado, TRUE);

        if (array_key_exists('error_message', $resultado)) {
            $this->error  = $resultado;
            $this->status = false;
        } else {
            $this->latitud  = $resultado['results'][0]['geometry']['location']['lat'];
            $this->logintud = $resultado['results'][0]['geometry']['location']['lng'];
            $this->status   = true;
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * @return mixed
     */
    public function getLogintud()
    {
        return $this->logintud;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }
}