#Gmaps Location Api Symfony Bundle

##Requerimientos

- [Symfony 2.8+]

##Instalación

    composer require tmwk/gmaps_geolocation
    
Modifica el archivo app/AppKernel.php para registrar el nuevo Bundle
    
    $bundles = array(
        // ...
        new Tmwk\GMapsBundle\TmwkGMapsBundle(),
        // ...
    );
    
Agrega a tu archivo app/config/config.yml lo siguiente:

    tmwk_g_maps:
        api_key: "%gmaps_api_key%"
        
Agrega a tu archivo app/config/parameters.yml lo siguiente:

    gmaps_api_key: 'code'

           

##Uso

###Ejemplo 

Obtenemos la latitud y longitud de la direcció

    $lat = $this->get('gmap.lat_long');
    
    $result = $lat->address('Direccion', 'Comuna', 'Ciudad');
    dump($result->getLatitud());
